# Upload files To SharePoint VSTS Build Step #

This extension enables you to upload a single file or the contents of a folder (including sub folders) to a SharePoint document library.

It supports uploads to both on-premise SharePoint farms as well as SharePoint Online tenants.

# Release Notes
## 1.2.8 (20-Aug-2018)
- Added URI reference for source code repository
## 1.2.7 (23-Jul-2018)
- Support for uploading to on-premise SharePoint site collections
## 1.1.10 (15-Nov-2017)
- Added support for deploying to a specified folder path
## 1.1.10 (21-Apr-2017)
- Fixed "instanceName" for more friendly and descriptive text
